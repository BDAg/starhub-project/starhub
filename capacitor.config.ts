import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.sigma.starhub',
  appName: 'starhub',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
