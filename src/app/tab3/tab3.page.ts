import { Component, OnInit } from '@angular/core';
import { OffersService } from '../services/offers.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{
  public hardware = [
    { val: 'Cooler e Watercooler', isChecked: false },
    { val: 'Disco rígido ', isChecked: false },
    { val: 'Fonte', isChecked: false },
    { val: 'Gabinete', isChecked: false },
    { val: 'Memória RAM', isChecked: false },
    { val: 'Memória SSD', isChecked: false },
    { val: 'Placa mãe', isChecked: false },
    { val: 'Placa de Som', isChecked: false },
    { val: 'Placa de Video', isChecked: false },
    { val: 'Processador', isChecked: false }
  ];
  public games = [
    { val: 'PC', isChecked: false },
    { val: 'Playstation 3', isChecked: false },
    { val: 'Playstation 4', isChecked: false },
    { val: 'Playstation 5', isChecked: false },
    { val: 'Nintendo', isChecked: false },
    { val: 'Xbox one', isChecked: false },
    { val: 'Xbox 360', isChecked: false }
  ];
  public perifericos = [
    { val: 'Adaptadores', isChecked: false },
    { val: 'Armazenamento', isChecked: false },
    { val: 'Cabos', isChecked: false },
    { val: 'Caixa de Som', isChecked: false },
    { val: 'Energia', isChecked: false },
    { val: 'Fone de Ouvido', isChecked: false },
    { val: 'Impressoras', isChecked: false },
    { val: 'Monitor', isChecked: false },
    { val: 'Mesa Digitalizadora', isChecked: false },
    { val: 'Mouse', isChecked: false },
    { val: 'Mousepad', isChecked: false },
    { val: 'Pendrives', isChecked: false },
    { val: 'Teclado', isChecked: false },
    { val: 'Webcam', isChecked: false }
  ];
  public variados = [
    { val: 'Acessórios de Simuladores', isChecked: false },
    { val: 'Cadeira Gamer', isChecked: false },
    { val: 'Óculos VR', isChecked: false },
    { val: 'Mesa Gamer', isChecked: false }
  ];
  ofertas = [];

  constructor(public offersService: OffersService) {}

  ngOnInit() {
    this.loading(true);
    this.offersService.getOffers('Nova Era Games').subscribe((result: any) => {
      this.ofertas = result;
      this.loading(false);
    });
  }

  loading(load: boolean) {
    if (load) {
      document.getElementById('body').style.overflow = 'hidden';
      document.getElementById('body').style.height = '100%';
      document.getElementById('loading').className = 'loading';
    } else {
      document.getElementById('body').style.overflow = 'scroll';
      document.getElementById('body').style.height = 'auto';
      document.getElementById('loading').className = 'invisible';
    }
  }

}
