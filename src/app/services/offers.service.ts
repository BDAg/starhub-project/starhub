import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OffersService {
  private readonly api: string = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  getOffers(param) {
    return this.http.get(this.api+'offers/'+param);
  }
}
